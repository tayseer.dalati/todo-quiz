import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todo_quiz/data/db_helper.dart';
import 'package:todo_quiz/data/prefs_helper.dart';
import 'package:todo_quiz/data/repository.dart';
import 'package:todo_quiz/screens/home/bloc/home_bloc.dart';
import 'package:todo_quiz/screens/login/bloc/login_bloc.dart';
import 'package:todo_quiz/screens/splash/bloc/splash_bloc.dart';

final sl = GetIt.instance;

Future<void> init() async {
  /// External
  final database = await initDatabase();
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => database);
  sl.registerLazySingleton(
    () {
      final dio = Dio(
        BaseOptions(
          connectTimeout: 30000,
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          responseType: ResponseType.plain,
        ),
      );
      dio.interceptors.add(
        LogInterceptor(
          responseBody: true,
          requestBody: true,
          responseHeader: true,
          requestHeader: true,
          request: true,
        ),
      );
      return dio;
    },
  );

  /// CORE
  sl.registerLazySingleton(() => PrefsHelper(sl()));
  sl.registerLazySingleton(() => DatabaseHelper(sl()));
  sl.registerLazySingleton(() => Repository(sl(), sl()));

  /// SCREENS BLOCS
  sl.registerFactory(() => SplashBloc(sl()));
  sl.registerFactory(() => LoginBloc(sl()));
  sl.registerFactory(() => HomeBloc(sl()));
}
