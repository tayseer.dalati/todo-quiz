part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoginError extends LoginState {}

class LoginSucceed extends LoginState {}

class LoggingIn extends LoginState {}
