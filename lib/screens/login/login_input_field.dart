import 'package:flutter/material.dart';
import 'package:todo_quiz/utils/colors.dart';

class LoginInputField extends StatelessWidget {
  final String title;
  final bool obscureText;
  final VoidCallback onSubmit;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final TextEditingController controller;

  LoginInputField({
    this.title,
    this.obscureText = false,
    this.onSubmit,
    this.focusNode,
    this.nextFocusNode,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      decoration: BoxDecoration(
        color: WHITE,
        borderRadius: BorderRadius.all(Radius.circular(16)),
        boxShadow: [
          BoxShadow(
            color: LIGHT_GREY,
            spreadRadius: 2,
            blurRadius: 4,
          ),
        ],
      ),
      child: TextField(
        controller: controller,
        obscureText: obscureText,
        focusNode: focusNode,
        textInputAction:
        nextFocusNode == null ? TextInputAction.done : TextInputAction.next,
        onSubmitted: (_) {
          nextFocusNode != null ? nextFocusNode.nextFocus() : onSubmit();
        },
        style: TextStyle(
          fontSize: 15,
          color: DARK_GREY,
          fontWeight: FontWeight.w400,
          fontFamily: Theme.of(context).textTheme.headline1.fontFamily,
        ),
        decoration: InputDecoration(
          hintText: title,
          border: InputBorder.none,
          hintStyle: TextStyle(
            fontSize: 15,
            color: GREY,
            fontWeight: FontWeight.w600,
            fontFamily: Theme.of(context).textTheme.headline1.fontFamily,
          ),
        ),
      ),
    );
  }
}
