import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_quiz/data/entities/todo_item.dart';
import 'package:todo_quiz/injection_container.dart';
import 'package:todo_quiz/screens/home/task_editor_view.dart';
import 'package:todo_quiz/screens/home/bloc/home_bloc.dart';
import 'package:todo_quiz/screens/home/task_tile.dart';
import 'package:todo_quiz/utils/colors.dart';
import 'package:todo_quiz/utils/utils.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _listKey = GlobalKey<AnimatedListState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _bloc = sl<HomeBloc>();

  @override
  void initState() {
    super.initState();
    _bloc.add(GetTodoList());
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _bloc,
      listener: (_, state) => _handleBlocListener(state),
      child: Scaffold(
        key: _scaffoldKey,
        floatingActionButton: FloatingActionButton(
          backgroundColor: PRIMARY,
          onPressed: () => showTaskEditorBottomSheet(isNew: true),
          child: Icon(Icons.add, color: WHITE),
        ),
        appBar: AppBar(
          centerTitle: true,
          title: Text('MY TODO LIST'),
        ),
        body: Column(
          children: [
            Expanded(
              child: BlocProvider(
                create: (_) => _bloc,
                child: BlocBuilder(
                  bloc: _bloc,
                  builder: (_, state) {
                    return (state is EmptyList)
                        ? emptyListView
                        : AnimatedList(
                            key: _listKey,
                            initialItemCount: _bloc.todoList.length,
                            itemBuilder: (context, index, animation) => slideIt(
                                context, _bloc.todoList[index], animation),
                          );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget get emptyListView => Center(
        child: InkWell(
          onTap: () => showTaskEditorBottomSheet(isNew: true),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Your todo list is empty!\nTap To add new',
              textAlign: TextAlign.center,
            ),
          ),
        ),
      );

  Widget slideIt(BuildContext context, TodoTask task, animation) {
    return SlideTransition(
      position: Tween<Offset>(
        begin: const Offset(-1, 0),
        end: Offset(0, 0),
      ).animate(animation),
      child: TaskTile(
        task: task,
        onRemove: () => _bloc.add(RemoveTask(task.id)),
        onEdit: () => showTaskEditorBottomSheet(isNew: false, task: task),
      ),
    );
  }

  void showTaskEditorBottomSheet({bool isNew, TodoTask task}) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext ctx) => TaskEditorView(
        isNew: isNew,
        task: task,
        onSubmit: (title, desc) => _bloc.add(isNew
            ? AddNewTask(title, desc)
            : EditTaskDetails(task.id, title, desc)),
      ),
    );
  }

  void _handleBlocListener(state) async {
    if (state is InsertAllItems) {
      for (int index = 0; index < _bloc.todoList.length; index++) {
        _listKey.currentState
            .insertItem(index, duration: const Duration(milliseconds: 300));
        await Future.delayed(Duration(milliseconds: 50));
      }
    } else if (state is InsertNewItem) {
      _listKey.currentState
          .insertItem(0, duration: const Duration(milliseconds: 300));
    } else if (state is RemoveListItem) {
      _listKey.currentState.removeItem(state.index,
          (_, animation) => slideIt(context, state.task, animation),
          duration: const Duration(milliseconds: 0));
    } else if (state is ShowError)
      showSnackBarMessage(_scaffoldKey.currentState, state.errorMessage);
  }
}
