
class PreferencesKeys {
  static const USERNAME = 'username';
  static const IS_LOGGED_IN = 'is_logged_in';
}

class DatabaseConstants {
  static const TABLE_TODO_ITEM = 'todo_item';

  static const COL_ID = 'id';
  static const COL_TITLE = 'title';
  static const COL_DESCRIPTION = 'description';
  static const COL_CREATION_DATE = 'creation_date';
  static const COL_HAS_DONE = 'has_done';
}

const LOGIN_USERNAME = 'username';
const LOGIN_PASSWORD = 'password';